using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    [SerializeField] GameObject bala;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) Fire();
    }

    private void Fire()
    {
        Vector3 balaPosition = new Vector3(transform.position.x, (transform.position.y + 1.5f), transform.position.z);
        Instantiate(bala, balaPosition, Quaternion.identity);
    }

}
