using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    [SerializeField] private int lifes;
    [SerializeField] GameObject bala;

    GameObject score;

    [SerializeField] private float _timeBetweenBullets;
    private float _bulletCooldown;

    void Start()
    {
        score = GameObject.Find("ScoreManager");
        _bulletCooldown = _timeBetweenBullets;
    }

    // Update is called once per frame
    void Update()
    {
        if (_bulletCooldown <= 0)
        {
            Vector3 enemyPosition = new Vector3(this.transform.position.x,
            this.transform.position.y - 1f, this.transform.position.z);

           Instantiate(bala, enemyPosition, Quaternion.identity);

            _bulletCooldown = _timeBetweenBullets;
        }
        else _bulletCooldown -= Time.deltaTime;


        if (lifes <= 0)
        {
            Destroy(this.gameObject);
            score.GetComponent<ScoreManager>().score += 20;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Bala"))
        {
            lifes--;
            Destroy(collision.gameObject);
        }

        if (collision.collider.CompareTag("Limit"))
        {
            Destroy(this.gameObject);
        }
    }
}
