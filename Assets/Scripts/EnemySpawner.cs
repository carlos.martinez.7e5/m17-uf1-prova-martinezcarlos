using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject enemy1;
    [SerializeField] GameObject enemy2;
    [SerializeField] float timeBetweenEnemies;
    float cooldown;


    // Start is called before the first frame update
    void Start()
    {
        cooldown = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown <= 0)
        {
            float randomX = Random.Range(-8f, 8f);
            float randomEnemy = Random.Range(0, 10);

            
            Vector3 enemyPosition = new Vector3(randomX,
            this.transform.position.y, this.transform.position.z);
            
            if(randomEnemy != 1) Instantiate(enemy1, enemyPosition, Quaternion.identity);
            else Instantiate(enemy2, enemyPosition, Quaternion.identity);

            cooldown = timeBetweenEnemies;
        }
        else cooldown -= Time.deltaTime;
    }
}
