using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBala : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Limit")) Destroy(this.gameObject);
        if (collision.collider.CompareTag("Bala"))
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }
    }
}
