using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public int score;
    private GameObject scoreTxt;
    
    
    void Start()
    {
        scoreTxt = GameObject.Find("Score");
    }

    
    void Update()
    {
        scoreTxt.GetComponent<Text>().text = "Score: " + score.ToString();
    }
}
