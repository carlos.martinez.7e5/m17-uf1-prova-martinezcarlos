using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyMisile : MonoBehaviour
{

    GameObject score;

    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("ScoreManager");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Limit")) Destroy(this.gameObject);

        if (collision.collider.CompareTag("Bala"))
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);

            score.GetComponent<ScoreManager>().score += 5;
        }
    }
    
}
